#! /bin/tcsh

set N_ENGINES=32

#module avail
set PYTHON_MODULE="python/anaconda_python-3.4.0"
set INTEL_MPI_MODULE="intel/impi4.1"
module load $PYTHON_MODULE $INTEL_MPI_MODULE

set IPYTHON_DIR="/a/home/cc/tree/taucc/students/chemist/inonshar/.ipython"
/usr/local/bin/mpiexec -n $N_ENGINES ipengine --profile-dir=$IPYTHON_DIR/profile_pbs
~
