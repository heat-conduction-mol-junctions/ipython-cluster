from cluster_helper.cluster import cluster_view
import test
import sys

def testRemote(num_jobs, args):
    with cluster_view(scheduler="torque", queue="nano1", num_jobs=num_jobs) as view:
        view.map(test.test, args)


def testLocal(num_jobs, args):
    with cluster_view(
        scheduler=None, 
        queue=None, 
        num_jobs=num_jobs, 
        extra_params={"run_local": True}
        ) as view:

        view.map(test.test, args)

def main():
    args=sys.argv[1:]
    num_jobs = 5
    testLocal(num_jobs, args)
    testRemote(num_jobs, args)

if __name__ == "__main__":
    main()
