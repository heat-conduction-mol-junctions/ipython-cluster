#! /bin/bash

# https://github.com/ipython/ipython/wiki/Cookbook:-Connecting-to-a-remote-kernel-via-ssh
# http://ipython.org/ipython-doc/dev/parallel/parallel_process.html#using-ipcluster-in-pbs-mode

# $ ipython profile create --parallel --profile=pbs

export BASH_MODULE="/usr/local/Modules/3.2.10/init/bash"
source $BASH_MODULE

export IPYTHON_MODULE="python/anaconda_python-3.4.0"
export INTEL_MPI_MODULE="intel/impi4.1"
module load $INTEL_MPI_MODULE $IPYTHON_MODULE

# start cluster with default no. of engines (32, ppn=16)
ipcluster start --profile=pbs
