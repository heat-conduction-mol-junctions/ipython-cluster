#! /bin/tcsh

# module avail
set PYTHON_MODULE="python/anaconda_python-3.4.0"
module load $PYTHON_MODULE

set IPYTHON_DIR="/a/home/cc/tree/taucc/students/chemist/inonshar/.ipython"
ipcontroller --profile-dir=$IPYTHON_DIR/profile_pbs
